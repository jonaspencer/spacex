import { InMemoryCache, Reference, makeVar } from '@apollo/client';

export const cache: InMemoryCache = new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
            isLoggedIn: {
                read() {
                  return isLoggedInVar();
                }
              },
              cartItems: {
                read() {
                  return cartItemsVar();
                }
              },
          launches: {
            keyArgs: false,
            merge(existing, incoming) {
              let launches: Reference[] = [];
              if (existing && existing.launches) {
                launches = launches.concat(existing.launches);
              }
              if (incoming && incoming.launches) {
                launches = launches.concat(incoming.launches);
              }
              return {
                ...incoming,
                launches,
              };
            }
          }
        }
      }
    }
  });
//This merge function takes our existing cached launches
//and the incoming launches and combines them into a single list,
//which it then returns. The cache stores this combined list and returns
//it to all queries that use the launches field.

// Initializes to true if localStorage includes a 'token' key,
// false otherwise
export const isLoggedInVar = makeVar<boolean>(!!localStorage.getItem('token'));

// Initializes to an empty array
export const cartItemsVar = makeVar<string[]>([]);
