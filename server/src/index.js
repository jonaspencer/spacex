require('dotenv').config();
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0
const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');

const { createStore } = require('./utils');
const resolvers = require('./resolvers');

const LaunchAPI = require('./datasources/launch');
const UserAPI = require('./datasources/user');


const store = createStore();

const isEmail = require('isemail');

const server = new ApolloServer({
  //Get the auth header value if there is one, decode it, and then check the database for the email
  context: async ({ req }) => {
    // simple auth check on every request
    const auth = req.headers && req.headers.authorization || '';
    const email = Buffer.from(auth, 'base64').toString('ascii');
    if (!isEmail.validate(email)) return { user: null };
    // find a user by their email
    const users = await store.users.findOrCreate({ where: { email } });
    const user = users && users[0] || null;
    return { user: { ...user.dataValues } };
  },
  typeDefs,
  resolvers, 

  dataSources: () => ({
    launchAPI: new LaunchAPI(),
    userAPI: new UserAPI({ store })
  })
});

server.listen().then(() => {
  console.log(`
    Server is running!
    Listening on port 4000
    Explore at https://studio.apollographql.com/sandbox
  `);
});

//APOLLO_KEY=service:spacelaunch-i3e7os:yxHi-_cHyGZ8POe2ap7RDQ
//APOLLO_GRAPH_REF=spacelaunch-i3e7os@current
//APOLLO_SCHEMA_REPORTING=true

